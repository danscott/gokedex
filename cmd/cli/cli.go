package main

import (
	"log"
	"os"
	"path"
	"strconv"

	"gitlab.com/danscott/gokedex/pkg/cache"
	"gitlab.com/danscott/gokedex/pkg/model"
	"gitlab.com/danscott/gokedex/pkg/resources"
)

func printHelp() {
	log.Fatalf("cli [type] [id]")
}

func main() {
	if len(os.Args) < 3 {
		printHelp()
	}

	t := os.Args[1]

	idStr, err := strconv.ParseUint(os.Args[2], 10, 32)

	id := model.ResourceID{Type: t, ID: int(idStr)}

	if err != nil {
		printHelp()
	}

	var gh resources.Github
	wd, _ := os.Getwd()
	fileCache := cache.NewFileCache(path.Join(wd, ".cache"))
	memCache := cache.NewMemoryCache()

	ca := resources.NewCachedAPI(gh, &fileCache, &memCache)

	data, err := ca.FetchResource(id)
	if err != nil {
		log.Fatalf("%v", err)
	}

	log.Printf("%s: %s", t, data[:20])

	log.Printf("and again")

	data, err = ca.FetchResource(id)
	if err != nil {
		log.Fatalf("%v", err)
	}

	log.Printf("%s: %s", t, data[:20])

	os.Exit(0)
}
