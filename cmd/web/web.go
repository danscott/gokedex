package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"gitlab.com/danscott/gokedex/pkg/server"

	"gitlab.com/danscott/gokedex/pkg/cache"
	"gitlab.com/danscott/gokedex/pkg/resources"
)

func main() {
	cloneDir := fmt.Sprintf("%s/personal/api-data", os.Getenv("HOME"))
	log.Printf("clone dir: %s", cloneDir)
	lc := resources.NewLocalCloneAPI(cloneDir)
	memCache := cache.NewMemoryCache()

	ca := resources.NewCachedAPI(lc, &memCache)

	s := server.NewServer(&ca)

	http.Handle("/", s.Handle())

	log.Fatalf("Team goket is panicing off again... %v", http.ListenAndServe(":8080", nil))
}
