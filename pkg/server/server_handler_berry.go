package server

import (
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"

	"gitlab.com/danscott/gokedex/pkg/model"
)

func handleBerry() func(data []byte, w http.ResponseWriter) {

	tmplStr := `
<h2>{{.Name}}</h2>
<ul>
	<li>{{.Firmness | asResLink}}</li>
</ul>
`
	funcMap := template.FuncMap{"asResLink": asResLink}

	tmpl, _ := template.New("berry").Funcs(funcMap).Parse(tmplStr)

	return func(data []byte, w http.ResponseWriter) {
		log.Print("Handling berry!")
		var b model.Berry
		if err := json.Unmarshal(data, &b); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Printf("Error reading berry data: %v", err)
		}
		w.WriteHeader(http.StatusOK)
		err := tmpl.Execute(w, b)
		if err != nil {
			log.Printf("%v", err)
		}

		w.Write(data)
	}

}
