package server

import (
	"compress/gzip"
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
	"gitlab.com/danscott/gokedex/pkg/model"
	"gitlab.com/danscott/gokedex/pkg/resources"
)

type Server struct {
	resources resources.ResourceFetcher
	rHandlers map[string]func(data []byte, w http.ResponseWriter)
}

func NewServer(resources resources.ResourceFetcher) Server {
	s := Server{resources, make(map[string]func(data []byte, w http.ResponseWriter))}
	s.rHandlers["berry"] = handleBerry()
	return s
}

func (s *Server) Handle() http.Handler {
	r := mux.NewRouter()
	r.HandleFunc("/", s.handleDefault)
	r.HandleFunc("/bulk", s.handleBulk)
	r.HandleFunc("/{t}/{id:[0-9]+}", s.handleResource)
	r.HandleFunc("/{t}", s.handleResourceList)

	return r
}

func (s *Server) handleDefault(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html")
	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, "<h1>...What?</h1>")
}

func (s *Server) handleResource(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodOptions {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET")
		w.Header().Set("Access-Control-Allow-Headers", "content-type")
		w.WriteHeader(http.StatusOK)
		return
	}

	vars := mux.Vars(r)
	t := vars["t"]
	idString := vars["id"]
	idInt, _ := strconv.ParseInt(idString, 10, 64)

	id := model.ResourceID{Type: t, ID: int(idInt)}

	data, err := s.resources.FetchResource(id)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		w.Header().Set("Content-Type", "text/plain")
		fmt.Fprintf(w, "Failed to fetch resource: %v", err)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(http.StatusOK)
	w.Write(data)
}

func (s *Server) handleResourceList(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodOptions {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET")
		w.Header().Set("Access-Control-Allow-Headers", "content-type")
		w.WriteHeader(http.StatusOK)
		return
	}

	vars := mux.Vars(r)
	t := vars["t"]

	data, err := s.resources.FetchResourceList(t)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		w.Header().Set("Content-Type", "text/plain")
		fmt.Fprintf(w, "Failed to get resource list: %v", err)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(http.StatusOK)
	w.Write(data)
}

func (s *Server) handleBulk(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodOptions {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "POST")
		w.Header().Set("Access-Control-Allow-Headers", "content-type")
		w.WriteHeader(http.StatusOK)
		return
	}

	decoder := json.NewDecoder(r.Body)
	var rList []model.ResourceID
	if err := decoder.Decode(&rList); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Header().Set("Content-Type", "text/plain")
		fmt.Fprintf(w, "Bad reqest: %v", err)
		return
	}

	dataList := make([]string, len(rList), len(rList))
	for i, id := range rList {
		data, err := s.resources.FetchResource(id)
		if err == nil {
			dataList[i] = string(data)
		} else {
			log.Printf("Error trying to get dat %v: %v", i, err)
		}
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Encoding", "gzip")
	w.WriteHeader(http.StatusOK)
	gz := gzip.NewWriter(w)
	defer gz.Close()
	fmt.Fprintf(gz, "[%s]", strings.Join(dataList, ","))
}

func asResLink(r model.NamedAPIResource) template.HTML {
	var link string
	id, err := model.ParseResourceId(r.URL)
	if err != nil {
		link = r.Name
	} else {
		link = fmt.Sprintf("<a href=\"/%s/%v\">%s</a>", id.Type, id.ID, r.Name)
	}
	return template.HTML(link)
}
