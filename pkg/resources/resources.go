package resources

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"gitlab.com/danscott/gokedex/pkg/cache"
	"gitlab.com/danscott/gokedex/pkg/model"
)

type ResourceFetcher interface {
	FetchResource(id model.ResourceID) ([]byte, error)
	FetchResourceList(resourceType string) ([]byte, error)
}

const (
	githubRootUrl = "https://raw.githubusercontent.com/PokeAPI/api-data/master/data/api/v2/"
)

type Github struct{}

func (Github) FetchResource(id model.ResourceID) ([]byte, error) {
	log.Printf("Fetching %v from github", id)

	url := fmt.Sprintf("%s%s/%v/index.json", githubRootUrl, id.Type, id.ID)

	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode == 404 {
		return nil, fmt.Errorf("%v does not exist", id)
	}

	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("Expected 200 response but got %v", resp.StatusCode)
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return data, nil
}

type LocalCloneAPI struct {
	rootDir string
}

func NewLocalCloneAPI(rootDir string) *LocalCloneAPI {
	l := LocalCloneAPI{rootDir}
	return &l
}

func (a *LocalCloneAPI) FetchResource(id model.ResourceID) ([]byte, error) {
	log.Printf("Fetching %v from local clone", id)

	fname := fmt.Sprintf("%s/data/api/v2/%s/%v/index.json", a.rootDir, id.Type, id.ID)

	return ioutil.ReadFile(fname)
}

func (a *LocalCloneAPI) FetchResourceList(resourceType string) ([]byte, error) {
	log.Printf("Fetching %s list from local clone", resourceType)

	fname := fmt.Sprintf("%s/data/api/v2/%s/index.json", a.rootDir, resourceType)

	return ioutil.ReadFile(fname)
}

type CachedAPI struct {
	c cache.Cacher
	f ResourceFetcher
}

func NewCachedAPI(fetcher ResourceFetcher, caches ...cache.Cacher) CachedAPI {
	var cachedAPI CachedAPI
	for i, c := range caches {
		if i == 0 {
			cachedAPI = CachedAPI{c, fetcher}
			continue
		}
		prevAPI := cachedAPI
		cachedAPI = CachedAPI{c, &prevAPI}
	}
	return cachedAPI
}

func (a *CachedAPI) FetchResource(id model.ResourceID) ([]byte, error) {
	if !a.c.IsInCache(id) {
		data, err := a.f.FetchResource(id)
		if err != nil {
			return nil, err
		}

		a.c.WriteToCache(data, id)

		return data, nil
	}
	return a.c.ReadFromCache(id)
}

func (a *CachedAPI) FetchResourceList(resourceType string) ([]byte, error) {
	return a.f.FetchResourceList(resourceType)
}
