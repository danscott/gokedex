package cache

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path"

	"gitlab.com/danscott/gokedex/pkg/model"
)

type CacheWriter interface {
	WriteToCache(data []byte, id model.ResourceID) error
}

type CacheReader interface {
	ReadFromCache(id model.ResourceID) ([]byte, error)
}

type CacheChecker interface {
	IsInCache(id model.ResourceID) bool
}

type Cacher interface {
	CacheWriter
	CacheReader
	CacheChecker
}

type FileCache struct {
	rootDir string
}

func NewFileCache(rootDir string) FileCache {
	return FileCache{rootDir}
}

func (c *FileCache) getStorePath(id model.ResourceID) string {
	fname := fmt.Sprintf("%05d.json", id.ID)
	return path.Join(c.rootDir, id.Type, fname)
}

func (c *FileCache) WriteToCache(data []byte, id model.ResourceID) error {
	log.Printf("Writing %v to file cache", id)

	fname := c.getStorePath(id)
	p := path.Dir(fname)

	if _, err := os.Stat(p); os.IsNotExist(err) {
		os.MkdirAll(p, 0755)
	}

	f, err := os.Create(fname)
	if err != nil {
		return err
	}

	if _, err := f.Write(data); err != nil {
		return err
	}

	return nil
}

func (c *FileCache) ReadFromCache(id model.ResourceID) ([]byte, error) {
	log.Printf("Reading %v from file cache", id)

	fname := c.getStorePath(id)

	if _, err := os.Stat(fname); os.IsNotExist(err) {
		return nil, err
	}

	return ioutil.ReadFile(fname)
}

func (c *FileCache) IsInCache(id model.ResourceID) bool {
	log.Printf("Checking for %v in file cache", id)
	fname := c.getStorePath(id)

	f, err := os.Stat(fname)

	return err == nil && !f.IsDir()
}

type MemoryCache struct {
	store map[string]map[int][]byte
}

func NewMemoryCache() MemoryCache {
	return MemoryCache{make(map[string]map[int][]byte)}
}

func (c *MemoryCache) getTypeMap(id model.ResourceID) *map[int][]byte {
	typeMap := c.store[id.Type]
	if typeMap == nil {
		typeMap = make(map[int][]byte)
		c.store[id.Type] = typeMap
	}
	return &typeMap
}

func (c *MemoryCache) WriteToCache(data []byte, id model.ResourceID) error {
	log.Printf("Writing %v to memory cache", id)
	(*c.getTypeMap(id))[id.ID] = data
	return nil
}

func (c *MemoryCache) ReadFromCache(id model.ResourceID) ([]byte, error) {
	log.Printf("Reading %v from memory cache", id)
	data := (*c.getTypeMap(id))[id.ID]
	if data == nil {
		return nil, fmt.Errorf("Resource %v is not in cache", id)
	}
	return data, nil
}

func (c *MemoryCache) IsInCache(id model.ResourceID) bool {
	log.Printf("Checking for %v in memory cache", id)
	return (*c.getTypeMap(id))[id.ID] != nil
}
