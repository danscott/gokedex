package model

type Berry struct {
	NamedIDResource
	GrowthTime       int              `json:"growth_time"`
	MaxHarvest       int              `json:"max_harvest"`
	NaturalGiftPower int              `json:"natural_gift_power"`
	Size             int              `json:"size"`
	Smoothness       int              `json:"smoothness"`
	SoilDryness      int              `json:"soil_dryness"`
	Firmness         NamedAPIResource `json:"firmness"`
	Flavors          []BerryFlavorMap `json:"flavors"`
	Item             NamedAPIResource `json:"item"`
	NaturalGiftType  NamedAPIResource `json:"natural_gift_type"`
}

type BerryFlavorMap struct {
	Potency int              `json:"potency"`
	Flavor  NamedAPIResource `json:"flavor"`
}

type BerryFirmness struct {
	NamedIDResource
	NameList
	Berries []NamedAPIResource `json:"berries"`
}

type BerryFlavor struct {
	NamedIDResource
	NameList
	Berries     []FlavorBerryMap `json:"berries"`
	ContestType NamedAPIResource `json:"contest_type"`
}

type FlavorBerryMap struct {
	Potency int              `json:"potency"`
	Berry   NamedAPIResource `json:"berry"`
}
