package model

import (
	"fmt"
	"strconv"
	"strings"
)

type APIResource struct {
	URL string `json:"url"`
}

type Description struct {
	Description string `json:"description"`
	LanguageResource
}

type Effect struct {
	Effect string `json:"effect"`
	LanguageResource
}

type Encounter struct {
	MinLevel        int                `json:"min_level"`
	MaxLevel        int                `json:"max_level"`
	ConditionValues []NamedAPIResource `json:"condition_values"`
	Chance          int                `json:"chance"`
	Method          NamedAPIResource   `json:"method"`
}

type FlavorText struct {
	FlavorText string `json:"flavor_text"`
	LanguageResource
}

type GenerationGameIndex struct {
	GameIndex  int              `json:"game_index"`
	Generation NamedAPIResource `json:"generation"`
}

type IDResouce struct {
	ID int `json:"id"`
}

type LanguageResource struct {
	Language NamedAPIResource `json:"language"`
}

type Name struct {
	NameResource
	LanguageResource
}

type NameList struct {
	Names []Name `json:"names"`
}

type NameResource struct {
	Name string `json:"name"`
}

type NamedAPIResource struct {
	APIResource
	NameResource
}

type NamedIDResource struct {
	IDResouce
	NameResource
}

type ResourceID struct {
	Type string `json:"type"`
	ID   int    `json:"id"`
}

func (r ResourceID) String() string {
	return fmt.Sprintf("%s-%v", r.Type, r.ID)
}

func ParseResourceId(u string) (ResourceID, error) {
	url := strings.Trim(u, "/")
	parts := strings.Split(url, "/")
	idPart := parts[len(parts)-1]
	id, err := strconv.ParseInt(idPart, 10, 64)
	if err != nil {
		return ResourceID{}, err
	}
	typePart := parts[len(parts)-2]

	return ResourceID{typePart, int(id)}, nil
}

type VerboseEffect struct {
	Effect      string `json:"effect"`
	ShortEffect string `json:"short_effect"`
	LanguageResource
}

type VersionEncounterDetail struct {
	Version          NamedAPIResource `json:"version"`
	MaxChance        int              `json:"max_chance"`
	EncounterDetails []Encounter      `json:"encounter_details"`
}

type VersionGameIndex struct {
	GameIndex int              `json:"game_index"`
	Version   NamedAPIResource `json:"version"`
}

type VersionGroupFlavorText struct {
	Text         string           `json:"text"`
	VersionGroup NamedAPIResource `json:"version_group"`
	LanguageResource
}
