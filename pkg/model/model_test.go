package model_test

import (
	"fmt"
	"testing"

	"gitlab.com/danscott/gokedex/pkg/model"
)

func TestParseResourceId(t *testing.T) {
	var testCases = []struct {
		url, t string
		id     int
	}{
		{"https://pokeapi.co/api/v2/region/1/", "region", 1},
		{"https://raw.githubusercontent.com/PokeAPI/api-data/master/data/api/v2/item/30/", "item", 30},
		{"/api/v2/effect/1000", "effect", 1000},
	}

	for _, c := range testCases {
		t.Run(fmt.Sprintf("Parse %s", c.url), func(t *testing.T) {
			expectedId := model.ResourceID{Type: c.t, ID: c.id}
			resourceId, err := model.ParseResourceId(c.url)
			if err != nil {
				t.Errorf("Expected %+v, got error %v", expectedId, err)
			}
			if resourceId != expectedId {
				t.Errorf("Expected %+v, got %+v", expectedId, resourceId)
			}
		})
	}
}

func TestParseResourceId_Errors(t *testing.T) {
	var testCases = []struct{ name, url string }{
		{"Empty string", ""},
		{"resource only", "/api/v2/effect"},
	}

	for _, c := range testCases {
		t.Run(fmt.Sprintf("%s should fail", c.name), func(t *testing.T) {
			_, err := model.ParseResourceId(c.url)
			if err == nil {
				t.Errorf("Expected an error, but didn't get one")
			}
		})
	}
}
