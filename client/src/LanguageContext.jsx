import PropTypes from 'prop-types';
import React, { createContext, useState, useContext } from 'react';
import { useResourceListFetcher } from './resourceFetcher';

const defaultContext = {
  current: 'en',
  available: ['en'],
  setCurrent: () => undefined
};

const LanguageContext = createContext(defaultContext);

const LanguageProvider = ({ children }) => {
  const [current, setCurrent] = useState('en');
  const [available, setAvaliable] = useState(['en']);
  const { loading, results } = useResourceListFetcher('language');

  const names = results.map(({ name }) => name);
  names.sort();

  if (!loading && names.toString() !== available.toString()) {
    setAvaliable(names);
  }

  return (
    <LanguageContext.Provider
      value={{
        current,
        available,
        setCurrent
      }}
    >
      {children}
    </LanguageContext.Provider>
  );
};

LanguageProvider.propTypes = {
  children: PropTypes.node.isRequired
};

const getLanguages = (obj, acc = []) => {
  if (Array.isArray(obj)) {
    return obj.reduce((arrAcc, item) => getLanguages(item, arrAcc), acc);
  }

  if (!obj || typeof obj !== 'object') {
    return acc;
  }

  if (obj.language) {
    return acc.concat(obj.language.name);
  }

  return Object.entries(obj).reduce((objAcc, [, val]) => getLanguages(val, objAcc), acc);
};

const useLanguageContext = () => {
  const language = useContext(LanguageContext);
  return {
    ...language,
    addAvailable: data => language.addAvailable(getLanguages(data))
  };
};

export { LanguageProvider, useLanguageContext };
