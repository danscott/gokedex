import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { TypeView } from './TypeView';
import { LanguageProvider } from './LanguageContext';
import { ListView } from './ListView';
import { ResourceTypes } from './ResourceTypes';

const getTypeParams = ({ match: { params } }) => params;

const App = () => (
  <LanguageProvider>
    <BrowserRouter>
      <Switch>
        <Route path="/:type/:id" render={props => <TypeView {...getTypeParams(props)} />} />
        <Route path="/:type" render={props => <ListView {...getTypeParams(props)} />} />
        <Route path="/" exact component={ResourceTypes} />
      </Switch>
    </BrowserRouter>
  </LanguageProvider>
);

export { App };
