import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useResourceListFetcher } from './resourceFetcher';
import { ResourceLink } from './Link';
import { Layout } from './Layout';
import { titleCase } from './utils/string';

const ListView = ({ type }) => {
  const { loading, results } = useResourceListFetcher(type);

  const title = `${loading ? 'Loading ' : ''}${titleCase(type)} list`;

  useEffect(
    () => {
      document.title = title;
    },
    [title]
  );

  const header = <h1>{title}</h1>;

  const body = loading ? null : (
    <ul>
      {results.map(resource => (
        <li key={resource.name}>
          <ResourceLink resource={resource} />
        </li>
      ))}
    </ul>
  );

  return <Layout header={header} body={body} />;
};

ListView.propTypes = {
  type: PropTypes.string.isRequired
};

export { ListView };
