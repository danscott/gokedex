import PropTypes from 'prop-types';
import React, { useEffect } from 'react';
import { Layout } from './Layout';
import { getName, useResourceFetcher } from './resourceFetcher';
import { TypeBody } from './TypeBody';
import { titleCase } from './utils/string';

function TypeView({ type, id }) {
  const { data, loading } = useResourceFetcher(type, id);

  const name = loading ? 'Loading...' : getName('en', data);

  const title = `${titleCase(type)} ${id}: ${name}`;

  useEffect(
    () => {
      document.title = title;
    },
    [title]
  );

  const header = (
    <div className="type-header">
      <h1>{title}</h1>
    </div>
  );

  const body = loading ? <div /> : <TypeBody data={data} />;

  return <Layout header={header} body={body} />;
}

TypeView.propTypes = {
  type: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired
};

export { TypeView };
