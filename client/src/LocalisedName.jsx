import React from 'react';
import PropTypes from 'prop-types';
import { useLanguageContext } from './LanguageContext';

const isLocalisable = ({ name, names }) => {
  const hasName = name !== null && name !== undefined;
  return Array.isArray(names) && hasName;
};

const LocalisedName = ({ localisable: { name, names } }) => {
  const { current } = useLanguageContext();
  if (names) {
    let locale = names.find(item => item.name && item.language.name === current);
    if (!locale) {
      locale = names.find(item => item.name && item.language.name === 'en');
    }
    if (locale) {
      return <span>{locale.name}</span>;
    }
  }
  return <span>{name}</span>;
};

LocalisedName.propTypes = {
  localisable: PropTypes.shape({
    name: PropTypes.string,
    names: PropTypes.arrayOf(PropTypes.object)
  }).isRequired
};

export { isLocalisable, LocalisedName };
