import PropTypes from 'prop-types';
import React from 'react';
import './TypeBody.scss';
import { ResourceLink } from './Link';
import { isLocalisable, LocalisedName } from './LocalisedName';
import { useLanguageContext } from './LanguageContext';

const isNamedResource = obj =>
  (Object.keys(obj).length === 2 &&
    Object.prototype.hasOwnProperty.call(obj, 'name') &&
    Object.prototype.hasOwnProperty.call(obj, 'url')) ||
  (Object.keys(obj).length === 1 && Object.prototype.hasOwnProperty.call(obj, 'url'));

const filterLocaleArray = (lang, arr) => {
  if (!arr.length || !arr[0].language) {
    return arr;
  }

  const byLang = l => ({ language }) => language.name === l;

  let list = arr.filter(byLang(lang));

  if (!list.length) {
    list = arr.filter(byLang('en'));
  }

  if (!list.length) {
    list = arr.filter(byLang(arr[0].language.name));
  }

  return list;
};

const ItemValue = ({ value }) => {
  const { current } = useLanguageContext();
  if (value === null) {
    return null;
  }
  if (isNamedResource(value)) {
    return <ResourceLink resource={value} />;
  }

  if (Array.isArray(value)) {
    return (
      <ul>
        {filterLocaleArray(current, value).map((data, i) => (
          // eslint-disable-next-line react/no-array-index-key
          <li key={i} className="resource-item-value">
            <ItemValue value={data} />
          </li>
        ))}
      </ul>
    );
  }

  if (typeof value === 'object') {
    return <TypeBody data={value} />;
  }

  return value.toString();
};

const Item = ({ field, value }) => (
  <li className="resource-item-value">
    <span>{field}: </span>
    <span>
      <ItemValue value={value} />
    </span>
  </li>
);

Item.propTypes = {
  field: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([PropTypes.object, PropTypes.array, PropTypes.string, PropTypes.number, PropTypes.bool])
};

Item.defaultProps = {
  value: null
};

const TypeBody = ({ data }) => {
  let entries = Object.entries(data);
  const localisable = isLocalisable(data);
  if (localisable) {
    entries = entries.filter(([key]) => key !== 'name' && key !== 'names');
  }
  return (
    <ul>
      {localisable && (
        <li key="name" className="resource-item-value">
          <span>Name: </span>
          <span>
            <LocalisedName localisable={data} />
          </span>
        </li>
      )}
      {entries.map(([key, value]) => (
        <Item key={key} field={key} value={value} />
      ))}
    </ul>
  );
};

TypeBody.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  data: PropTypes.object.isRequired
};

export { TypeBody };
