const titleCase = str => {
  if (!str) {
    return str;
  }
  return `${str[0].toUpperCase()}${str.slice(1)}`;
};

// eslint-disable-next-line import/prefer-default-export
export { titleCase };
