const BATCH_SIZE = 100;
const stack = [];
let throttleTimeout;
let runningCalls;

const runCalls = async () => {
  if (!stack.length) {
    return;
  }
  while (stack.length) {
    runningCalls = true;
    const stackCopy = stack.splice(0, BATCH_SIZE);
    const body = stackCopy.map(({ type, id }) => ({ type, id: parseInt(id, 10) }));
    try {
      // eslint-disable-next-line no-await-in-loop
      const response = await fetch(`http://localhost:8080/bulk`, {
        method: 'POST',
        mode: 'cors',
        body: JSON.stringify(body)
      });
      // eslint-disable-next-line no-await-in-loop
      const data = await response.json();
      // eslint-disable-next-line no-loop-func
      data.forEach((item, index) => {
        if (!item) {
          stackCopy[index].reject('missing');
        }
        stackCopy[index].resolve(item);
      });
    } catch (e) {
      stackCopy.forEach(({ reject }) => reject(e));
    } finally {
      runningCalls = false;
    }
  }
};

const getResource = async (type, id) => {
  const promise = new Promise((resolve, reject) => stack.push({ type, id, resolve, reject }));

  if (throttleTimeout) {
    clearInterval(throttleTimeout);
  }

  throttleTimeout = setInterval(() => {
    if (!runningCalls) {
      clearInterval(throttleTimeout);
      runCalls();
    }
  }, 500);

  return promise;
};

const getResourceList = async type => {
  const result = await fetch(`http://localhost:8080/${type}`, { mode: 'cors', method: 'GET' });
  const data = await result.json();
  return data.results;
};

export { getResource, getResourceList };
