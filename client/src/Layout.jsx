import PropTypes from 'prop-types';
import React from 'react';
import './Layout.scss';
import { SelectLanguage } from './SelectLanguage';

const Layout = ({ header, body }) => (
  <div className="layout">
    <div className="layout-header">
      {header}
      <div>
        <SelectLanguage />
      </div>
    </div>
    <div className="layout-body">{body}</div>
  </div>
);

Layout.propTypes = {
  header: PropTypes.node,
  body: PropTypes.node
};

Layout.defaultProps = {
  header: null,
  body: null
};

export { Layout };
