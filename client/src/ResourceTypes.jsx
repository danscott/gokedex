import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Layout } from './Layout';
import { titleCase } from './utils/string';

const types = [
  'ability',
  'berry',
  'berry-firmness',
  'berry-flavor',
  'characteristic',
  'contest-effect',
  'contest-type',
  'egg-group',
  'encounter-condition',
  'encounter-condition-value',
  'encounter-method',
  'evolution-chain',
  'evolution-trigger',
  'gender',
  'generation',
  'growth-rate',
  'item',
  'item-attribute',
  'item-category',
  'item-fling-effect',
  'item-pocket',
  'language',
  'location',
  'location-area',
  'machine',
  'move',
  'move-ailment',
  'move-battle-style',
  'move-category',
  'move-damage-class',
  'move-learn-method',
  'move-target',
  'nature',
  'pal-park-area',
  'pokeathlon-stat',
  'pokedex',
  'pokemon',
  'pokemon-color',
  'pokemon-form',
  'pokemon-habitat',
  'pokemon-shape',
  'pokemon-species',
  'region',
  'stat',
  'super-contest-effect',
  'type',
  'version',
  'version-group'
];

const ResourceTypes = () => {
  useEffect(() => {
    document.title = 'Resource types';
  }, []);

  const header = <h1>Resource types</h1>;
  const body = (
    <ul>
      {types.map(type => (
        <li key={type}>
          <Link to={`/${type}`}>{titleCase(type)}</Link>
        </li>
      ))}
    </ul>
  );

  return <Layout header={header} body={body} />;
};

export { ResourceTypes };
