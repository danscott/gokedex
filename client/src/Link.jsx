import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { useResourceFetcher } from './resourceFetcher';
import { LocalisedName, isLocalisable } from './LocalisedName';

const TypeLink = ({ type, id, children }) => <Link to={`/${type}/${id}`}>{children}</Link>;

TypeLink.propTypes = {
  type: PropTypes.string.isRequired,
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  children: PropTypes.node.isRequired
};

const ResourceLink = ({ resource: { name, url } }) => {
  const [type, id] = url.split('/').slice(-3);
  const { data, loading } = useResourceFetcher(type, id);
  const localisable = isLocalisable(data);

  return (
    <TypeLink type={type} id={id}>
      {loading || !localisable ? name || `${type}-${id}` : <LocalisedName localisable={data} />}
    </TypeLink>
  );
};

ResourceLink.propTypes = {
  resource: PropTypes.shape({
    name: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired
  }).isRequired
};

export { ResourceLink, TypeLink };
