import { useEffect, useState } from 'react';
import { getResource, getResourceList } from './utils/resource';

const resourceCache = {};

const cacheKey = (type, id) => `${type}-${id}`;

const getCached = (type, id) => resourceCache[cacheKey(type, id)];
const hasCached = (type, id) => !!resourceCache[cacheKey(type, id)];
const setCached = (type, id, item) => {
  resourceCache[cacheKey(type, id)] = item;
};

const callApi = async (type, id) => {
  if (hasCached(type, id)) {
    return getCached(type, id);
  }

  let result;

  if (id) {
    result = await getResource(type, id);
  } else {
    result = await getResourceList(type);
  }

  setCached(type, id, result);

  return result;
};

const getName = (language, { name, names }) => {
  if (!names) {
    return name;
  }
  const localisedName = names.find(locale => locale.language.name === language);
  return localisedName ? localisedName.name : name;
};

const useResourceFetcher = (type, id) => {
  const [data, setData] = useState({});
  const [loading, setLoading] = useState(true);
  const fetchResource = async () => {
    if (hasCached(type, id)) {
      setData(getCached(type, id));
      if (loading) {
        setLoading(false);
      }
      return;
    }
    setLoading(true);
    const response = await callApi(type, id);
    setData(response);
    setLoading(false);
  };
  useEffect(
    () => {
      fetchResource();
    },
    [type, id]
  );
  return { data, loading };
};

const useResourceListFetcher = type => {
  const [results, setResults] = useState([]);
  const [loading, setLoading] = useState(true);
  const fetchList = async () => {
    if (hasCached(type)) {
      setResults(getCached(type));
      if (loading) {
        setLoading(false);
      }
      return;
    }
    setLoading(true);
    const response = await callApi(type);
    setResults(response);
    setLoading(false);
  };
  useEffect(
    () => {
      fetchList();
    },
    [type]
  );
  return { results, loading };
};

export { getName, useResourceFetcher, useResourceListFetcher };
