import React from 'react';
import { useLanguageContext } from './LanguageContext';

const SelectLanguage = () => {
  const { available, current, setCurrent } = useLanguageContext();

  const list = available.slice();

  if (list.indexOf(current) < 0) {
    list.unshift(current);
  }

  return (
    <select onChange={e => setCurrent(e.target.value)} value={current}>
      {list.map(lang => (
        <option key={lang} defaultValue={lang}>
          {lang}
        </option>
      ))}
    </select>
  );
};

export { SelectLanguage };
